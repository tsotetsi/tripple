const scss = require("./assets/scss/main.scss"),
  $ = require("jquery");
window.$ = $;
window.jQuery = $;
import what from "what-input";
import foundation from "../node_modules/foundation-sites/js/foundation";
import Foundation from "../src/assets/js/foundation";

import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

ReactDOM.render(<App />, document.getElementById("app"));
